### Problem summary

Given a FASTA (Pearson) formatted file containing sequence ids and their respective DNA sequences, find and return the sequence id and GC content of the sequence with the largest GC content

### Algorithm idea

Find and extract the FASTA ids by looking for '>'. Every ID is followed by the sequence. Extract this sequence into a separate list until either the next FASTA entry appears or an EOF is observed. Once the sequences are extracted, find the GC content of each sequence and return the FASTA id of the entry with the Greatest GC content

### Pseudo code

Here we can go ahead and implement this in python. I stole my idea from the FASTA parser that BioPython uses. I implemented two functions. One to return the FASTAids and Sequences, and another to return the FASTA id of the sequence with the greatest GC content.
```python
def fastaread(filehandle):

    # Initialize some arrays to return first
    fastaid = []
    seqarray = []

    # Open file
    f = openandread(handle)

    # First while True:/Break idiom to pass any and all blank spaces
    while True:
        line = f.readline
        if line is empty:
            return
        if line[0] is '>':           # If we find a fasta id however, then we break
            break

    # Now we'll start our other while True:/Break idiom
    # This one will find the fasta record, put that record in an array.
    # Then until the next record or end of file is encountered, it will
    # read in all the lines and return that sequence

    while True:
        if not line.startswith('>'):
            raise Error(
                    "Records in FASTA files should start with '>' character")

        title = line[1:end].rstrip()
        fastaid.append(title)

        # Initialize our seqdata variable. This one will store one single sequence
        seqdata = []

        line = f.readline()

        # This second while True:/Break idiom extract the sequence data
        while True:
            if not line:
                break
            if line [0] == '>':       # Check to see if we're onto the next record
                break
            seqdata.append(line.rstrip())
            line = f.readline()

        # Take the data in seqdata and join it together and add to the seqarray
        nts = ''.join(seqdata).replace(" ", "").replace("\r", "")
        seqarray.append(nts)

        # If we're at the end of the document we'll go ahead and return our array
        # of ids and sequences
        if not line:
            return fastaid, seqarray

def percentgc(fastaids, seqarray):

    # Finds the gc content of sequences in FASTA files

    # Initialize our seqgc variable
    # This stores all the %gc vals for seq
    seqgc = [];

    # First we need to check if the two arrays are of
    # the same length, i.e., have the same number of
    # elements in them

    if len(fastaids) != len(seqarray):
        return ValueError(
            "The number of records and sequences is not the same. Try again.")

    for seq in seqarray:
        gccount = seq.count('G') + seq.count('C')
        totcount = len(seq)
        seqgc.append(100*gccount/totcount)

    # Find the max value and index and return the fasta id and %gc
    max_gc = max(seqgc)
    max_index = seqgc.index(max_gc)

    print fastaids[max_index]
    print max_gc
```

### Complexity Analyses and Correctness

The fastaread function's main loop, where the while True:/break idiom reads the DNA sequences, scales linearly O(n). The lines.append is called each time a DNA sequence is found. This could become a problem if we have very large sequences since the running time is only bounded by the file size.

### Any advanced information

Why would we want to calculate GC content? Why does GC content even matter? At the prokaryotic level, GC content correlates coding-sequence length, correlates with certain secondary RNA structures, and there is also a noted bias towards low GC content in stop codons (TAG, TAA, and TGA). These are just to name a few situations where rich GC regions and GC poor regions correlate with functional significance. Long coding regions in vertebrates and prokaryotes are significantly correlated with GC content; long coding regions tend to be GC-rich where as short coding regions tend to be GC poor. Since codons are biased towards being AT rich, mutations in AT rich regions can likely lead to the generation of stop codons. Whereas in GC regions, many such mutations might be required to spontaneously lead to stop codons. Therefore, conserved regions across organisms are likely GC rich.

Oliver, JL and Marin, A. A relationship between GC content and coding-sequence length. J Mol Evol 1996 Sep;43(3)216-23

Andersson, SGE and Kurland CG. Genomic evolution drives the evolution of the translation system. Biochem. Cell Biol 73:775-787 (1995)

D'Onofrio, Giuseppe and Benardy, Giorgio. A Universal Compositional correlation among Codon Positions. GENE, 110(1992)81-88
