## Exercise 1: Choosing a clinical problem and framing it as a machine learning task.

In this exercise, I practiced how to choose a clinical problem to tackle, explore how to research some basic facts about the disease, and frame the clinical problem as a machine learning task.

### Part 1: Choosing a clinical case.

Exciting clinical problems abound in the realm of Healthcare AI. To help me narrow down some potential ideas, I used the American College of Radiology Data Science Institute (ACR DSI) curated list of use cases. If you are following this exercise from the comfort of your own workspace, kindly note that some use cases in the ACR DSI database can seem overwhelming or obscure without some background medical knowledge. If this is the case, simply choose something that’s more intuitive. A few good starting examples include:

Acute Appendicitis.
Aging Brain – Dementia.
Incidental Pulmonary Nodules on CT.

Part 1 -
* choose a case
Part 2 -
* Peform Background research
Part 3 -
* Framing the concern as a machine learning Exercise


My Case: COVID-19 Chest CT Pattern

-------

## prerequisites
* knowledge of machine learning
