# Help and Documentation

This documentation covers the following:
- Help
- Further Reading

# Help

# Accessing Documentation with `?`

Python has a built-in `help()` function that can access this information and prints the results. For example, to see the documentation of the built-in len function, you can do the following:


```python
In [1]: help(len)
Help on built-in function len in module builtins:

len(...)
    len(object) -> integer

    Return the number of items of a sequence or mapping.
```


Because finding help on an object is so common and useful, IPython introduces the `?` character as a shorthand for accessing this documentation and other relevant information:

```python
In [2]: len?
Type:        builtin_function_or_method
String form: <built-in function len>
Namespace:   Python builtin
Docstring:
len(object) -> integer

Return the number of items of a sequence or mapping.
```

This notation works for just about anything, including object methods:

```python
In [3]: L = [1, 2, 3]
In [4]: L.insert?
Type:        builtin_function_or_method
String form: <built-in method insert of list object at 0x1024b8ea8>
Docstring:   L.insert(index, object) -- insert object before index

```
or even objects themselves, with the documentation from their type:

```python
In [5]: L?
Type:        list
String form: [1, 2, 3]
Length:      3
Docstring:
list() -> new empty list
list(iterable) -> new list initialized from iterable's items
```
Importantly, this will even work for functions or other objects you create yourself! Here we'll define a small function with a docstring:

```python
In [6]: def square(a):
  ....:     """Return the square of a."""
  ....:     return a ** 2
  ....:
```
Note that to create a docstring for our function, we simply placed a string literal in the first line. Because doc strings are usually multiple lines, by convention we used Python's triple-quote notation for multi-line strings.

Now we'll use the `?` mark to find this doc string:

```python
In [7]: square?
Type:        function
String form: <function square at 0x103713cb0>
Definition:  square(a)
Docstring:   Return the square of a.
```
This quick access to documentation via docstrings is one reason you should get in the habit of always adding such inline documentation to the code you write!

# Accessing Source Code with  `??`

Because the Python language is so easily readable, another level of insight can usually be gained by reading the source code of the object you're curious about. IPython provides a shortcut to the source code with the double question mark (`??`): 
Because the Python language is so easily readable, another level of insight can usually be gained by reading the source code of the object you're curious about. IPython provides a shortcut to the source code with the double question mark (`??`):
```python
In [8]: square??
Type:        function
String form: <function square at 0x103713cb0>
Definition:  square(a)
Source:
def square(a):
    "Return the square of a"
    return a ** 2
```
For simple functions like this, the double question-mark can give quick insight into the under-the-hood details.

If you play with this much, you'll notice that sometimes the  `??`  suffix doesn't display any source code: this is generally because the object in question is not implemented in Python, but in C or some other compiled extension language. If this is the case, the  `??`  suffix gives the same output as the  `?`  suffix. You'll find this particularly with many of Python's built-in objects and types, for example  `len`  from above:
```python
In [9]: len??
Type:        builtin_function_or_method
String form: <built-in function len>
Namespace:   Python builtin
Docstring:
len(object) -> integer
```
Return the number of items of a sequence or mapping.

Using  `?`  and/or  `??`  gives a powerful and quick interface for finding information about what any Python function or module does.

# Exploring Modules with Tab-Completion

IPython's other useful interface is the use of the tab key for auto-completion and exploration of the contents of objects, modules, and name-spaces. In the examples that follow, we'll use  `<TAB>`  to indicate when the Tab key should be pressed.

## Tab-completion of object contents

Every Python object has various attributes and methods associated with it. Like with the  `help`  function discussed before, Python has a built-in  `dir`  function that returns a list of these, but the tab-completion interface is much easier to use in practice. To see a list of all available attributes of an object, you can type the name of the object followed by a period ("`.`") character and the Tab key:
```python
In [10]: L.<TAB>
L.append   L.copy     L.extend   L.insert   L.remove   L.sort     
L.clear    L.count    L.index    L.pop      L.reverse
```
To narrow-down the list, you can type the first character or several characters of the name, and the Tab key will find the matching attributes and methods:
```python
In [10]: L.c<TAB>
L.clear  L.copy   L.count  

In [10]: L.co<TAB>
L.copy   L.count
```

If there is only a single option, pressing the Tab key will complete the line for you. For example, the following will instantly be replaced with  `L.count`:
```python
In [10]: L.cou<TAB>
```
Though Python has no strictly-enforced distinction between public/external attributes and private/internal attributes, by convention a preceding underscore is used to denote such methods. For clarity, these private methods and special methods are omitted from the list by default, but it's possible to list them by explicitly typing the underscore:
```python
In [10]: L._<TAB>
L.__add__           L.__gt__            L.__reduce__
L.__class__         L.__hash__          L.__reduce_ex__
```

For brevity, we've only shown the first couple lines of the output. Most of these are Python's special double-underscore methods (often nicknamed "dunder" methods).

## Tab completion when importing

Tab completion is also useful when importing objects from packages. Here we'll use it to find all possible imports in the  `itertools`  package that start with  `co`:

```python
In [10]: from itertools import co<TAB>
combinations                   compress
combinations_with_replacement  count
```

Similarly, you can use tab-completion to see which imports are available on your system (this will change depending on which third-party scripts and modules are visible to your Python session):

```python
In [10]: import <TAB>
Display all 399 possibilities? (y or n)
Crypto              dis                 py_compile
Cython              distutils           pyclbr
...                 ...                 ...
difflib             pwd                 zmq

In [10]: import h<TAB>
hashlib             hmac                http         
heapq               html                husl
```

(Note that for brevity, I did not print here all 399 importable packages and modules on my system.)

## Beyond tab completion: wildcard matching

Tab completion is useful if you know the first few characters of the object or attribute you're looking for, but is little help if you'd like to match characters at the middle or end of the word. For this use-case, IPython provides a means of wildcard matching for names using the  `*`  character.

For example, we can use this to list every object in the namespace that ends with  `Warning`:

```python
In [10]: *Warning?
BytesWarning                  RuntimeWarning
DeprecationWarning            SyntaxWarning
FutureWarning                 UnicodeWarning
ImportWarning                 UserWarning
PendingDeprecationWarning     Warning
ResourceWarning
```
Notice that the  `*`  character matches any string, including the empty string.

Similarly, suppose we are looking for a string method that contains the word  `find`  somewhere in its name. We can search for it this way:
```python
In [10]: str.*find*?
str.find
str.rfind
```
I find this type of flexible wildcard search can be very useful for finding a particular command when getting to know a new package or reacquainting myself with a familiar one.


# Further reading

The following are some resources that you can use to become more familiar with Jupyter:

-   Jupyter Notebook Basics:  [https://nbviewer.jupyter.org/github/jupyter/notebook/blob/master/docs/source/examples/Notebook/Notebook%20Basics.ipynb](https://nbviewer.jupyter.org/github/jupyter/notebook/blob/master/docs/source/examples/Notebook/Notebook%20Basics.ipynb)
    
-   JupyterLab introduction:  [https://blog.jupyter.org/jupyterlab-is-ready-for-users-5a6f039b8906](https://blog.jupyter.org/jupyterlab-is-ready-for-users-5a6f039b8906)
    
-   Learning Markdown to make your Jupyter Notebooks presentation-ready:  [https://medium.com/ibm-data-science-experience/markdown-for-jupyter-notebooks-cheatsheet-386c05aeebed](https://medium.com/ibm-data-science-experience/markdown-for-jupyter-notebooks-cheatsheet-386c05aeebed)
    
-   28 Jupyter Notebook Tips, Tricks, and Shortcuts:  [https://www.dataquest.io/blog/jupyter-notebook-tips-tricks-shortcuts/](https://www.dataquest.io/blog/jupyter-notebook-tips-tricks-shortcuts/)
    

The following resource shows you how to use conda to manage virtual environments instead of the venv solution that was explained earlier in this chapter:

-   Managing virtual environments with Conda:  [https://medium.freecodecamp.org/why-you-need-python-environments-and-how-to-manage-them-with-conda-85f155f4353c](https://medium.freecodecamp.org/why-you-need-python-environments-and-how-to-manage-them-with-conda-85f155f4353c)

Some books on web scraping in Python and designing data visualizations for readability are as follows:

-   Information Dashboard Design: Displaying Data for At-a-Glance Monitoring, Second Edition by Stephen Few:  [https://www.amazon.com/Information-Dashboard-Design-At-Glance/dp/1938377001/](https://www.amazon.com/Information-Dashboard-Design-At-Glance/dp/1938377001/)
    
-   Web Scraping with Python: Collecting More Data from the Modern Web, 2nd Edition by Ryan Mitchell:  [https://www.amazon.com/Web-Scraping-Python-Collecting-Modern/dp/1491985577](https://www.amazon.com/Web-Scraping-Python-Collecting-Modern/dp/1491985577)
    

Some resources for learning more advanced concepts of statistics (that we won't cover here) and carefully applying them are as follows:

-   A Gentle Introduction to Normality Tests in Python:  [https://machinelearningmastery.com/a-gentle-introduction-to-normality-tests-in-python/](https://machinelearningmastery.com/a-gentle-introduction-to-normality-tests-in-python/)
    
-   How Hypothesis Tests Work: Confidence Intervals and Confidence Levels:  [https://statisticsbyjim.com/hypothesis-testing/hypothesis-tests-confidence-intervals-levels/](https://statisticsbyjim.com/hypothesis-testing/hypothesis-tests-confidence-intervals-levels/)
    
-   Intro to Inferential Statistics (Making Predictions with Data) on Udacity:  [https://www.udacity.com/course/intro-to-inferential-statistics--ud201](https://www.udacity.com/course/intro-to-inferential-statistics--ud201)
    
-   Penn State elementary statistics lesson 4 confidence intervals:  [https://newonlinecourses.science.psu.edu/stat200/lesson/4](https://newonlinecourses.science.psu.edu/stat200/lesson/4)
    
-   Seeing Theory: A visual introduction to probability and statistics:  [https://seeing-theory.brown.edu/index.html](https://seeing-theory.brown.edu/index.html)
    
-   Statistics Done Wrong: The Woefully Complete Guide by Alex Reinhart:  [https://www.amazon.com/Statistics-Done-Wrong-Woefully-Complete/dp/1593276206](https://www.amazon.com/Statistics-Done-Wrong-Woefully-Complete/dp/1593276206)
    
-   Survey Sampling Methods:  [https://stattrek.com/survey-research/sampling-methods.aspx](https://stattrek.com/survey-research/sampling-methods.aspx)


[MIT](https://choosealicense.com/licenses/mit/)
