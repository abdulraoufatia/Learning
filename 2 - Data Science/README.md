# Python Data Science 

This is my code repository for the book Python Data Science Handbook, published by O'Reilly Media, 2016. 

# Table of Contents

- [Chapter 1 - Introduction to Data Science](https://github.com/abdulraoufatia/Learning/tree/main/1%20-%20Learning%20Python/6%20-%20Data%20Science/1-%20Introduction%20to%20Data%20Science), covers the fundamentals of data analysis. It provides the foundation in statistics, and get the environment set up for working with data in Python and using Jupyter Notebooks.

- [Chapter 2 - NumPy](https://github.com/abdulraoufatia/Learning/tree/main/1%20-%20Learning%20Python/6%20-%20Data%20Science/2%20-%20NumPy), outlines techniques for effectively loading, storing, and manipulating in-memory data in Python. 


