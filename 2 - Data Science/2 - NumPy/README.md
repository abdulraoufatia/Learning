# Introduction to Chapter

This chapter will cover NumPy in detail. NumPy (short for Numerical Python) provides an efficient interface to store and operate on dense data buffers. In some ways, NumPy arrays are like Python's built-in list type, but NumPy arrays provide much more efficient storage and data operations as the arrays grow larger in size. NumPy arrays form the core of nearly the entire ecosystem of data science tools in Python, so time spent learning to use NumPy effectively will be valuable no matter what aspect of data science interests you.
## Importing NumPy

If you followed the advice outlined in the Preface and installed the Anaconda stack, you already have NumPy installed and ready to go. If you're more the do-it-yourself type, you can go to http://www.numpy.org/ and follow the installation instructions found there. Once you do, you can import NumPy and double-check the version:

```sh
import numpy
    numpy.__version__

# '1.11.1'
```


For the pieces of the package discussed here, I'd recommend NumPy version 1.8 or later. By convention, you'll find that most people in the SciPy/PyData world will import NumPy using np as an alias:

```sh
import numpy as np
```   

Throughout this chapter, and indeed the rest of the repository, you'll find that this is the way I will import and use NumPy.

# Reminder about Built In Documentation
As you read through this chapter, don't forget that IPython gives you the ability to quickly explore the contents of a package (by using the tab-completion feature), as well as the documentation of various functions (using the ? character – Refer back to [Help and Documentation](https://github.com/abdulraoufatia/Learning/blob/main/1%20-%20Learning%20Python/6%20-%20Data%20Science/1-%20Introduction%20to%20Data%20Science/README.md).

For example, to display all the contents of the numpy namespace, you can type this:

 ```sh
In [3]: np.<TAB>
```

And to display NumPy's built-in documentation, you can use this:

```sh
In [4]: np?
```

More detailed documentation, along with tutorials and other resources, can be found at http://www.numpy.org.
